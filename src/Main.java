import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        String innerText = """
                Если вам некомфортно в своём теле, вы не найдёте комфорта и в окружающем мире.\s
                Как говорит Ницше, тот, у кого есть зачем жить, легко выдержит любое «как».\s
                Вопросы, на которые вы не знаете ответа, гораздо лучше ответов, в которых вам не дозволено сомневаться.\s
                Историю делали очень немногие, а все остальные тем временем пахали землю и таскали воду вёдрами.\s
                С иллюзиями жить проще – они наполняют страдания смыслом.\s
                Человечество – не стая волков, завладевшая вдруг танками и атомными бомбами, скорее мы – стадо овец, которое в силу непонятной прихоти эволюции научилось делать и пускать в ход танки и ракеты. А вооружённые овцы гораздо опаснее вооружённых волков.\s
                Вы не уговорите мартышку поделиться с вами бананом, посулив ей сколько угодно бананов после смерти, в раю для мартышек.\s""";
        String[] stringWords = innerText.toLowerCase().replaceAll("[^A-Za-zА-Яа-я ]", "").split(" ");

        outerCount(stringWords);
    }

    public static void outerCount(String[] innerText) {
        Map<String, Integer> mapInnerText = new HashMap<>();
        for (String s : innerText) {
            if (mapInnerText.containsKey(s)) {
                mapInnerText.put(s, mapInnerText.get(s) + 1);
            } else {
                mapInnerText.put(s, 1);
            }
        }
        mapInnerText.remove("");
        for (String str: mapInnerText.keySet()) {
            System.out.println(str + " - " + mapInnerText.get(str) + " раз");
        }

    }
}



