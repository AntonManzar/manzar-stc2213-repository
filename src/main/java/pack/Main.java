package pack;

public class Main {
    public static void main(String[] args) {
        Human humanOne = new Human("Иван", "Бунин", "Сергеевич", "Санкт-Петербург", "Парашютная",
                "1", "5", "0303", "434565");
        Human humanTwo = new Human("Андрей", "Согомонян", "Варанович", "Москва", "Первая",
                "64", "1765", "0573", "346744");
        Human humanThree = new Human("Даниил", "Антонов", "Неизвестнович", "Анапа", "Ленина",
                "43", "181", "0303", "434565");

        System.out.println(humanOne.equals(humanTwo));
        System.out.println(humanTwo.equals(humanThree));
        System.out.println(humanThree.equals(humanOne));
        System.out.println();
        System.out.println(humanOne + "\n");
        System.out.println(humanTwo + "\n");
        System.out.println(humanThree + "\n");
    }
}